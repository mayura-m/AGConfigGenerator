﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(AGConfigGenerator.Startup))]
namespace AGConfigGenerator
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
